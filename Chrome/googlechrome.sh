#!/bin/bash

# This script will download and install Google Chrome on a fresh installation of Mac OS X.
# Usage: curl -fkL gist.github.com/raw/4364590/install-chrome.sh | sh
# Running artisinal bash install scripts is generally not recommended.
# your funeral
# v2017.03.20 - Matthew Bodaly

DOWNLOAD_URL="https://dl.google.com/chrome/mac/stable/GGRO/googlechrome.dmg"
loggedInUser=`python -c 'from SystemConfiguration import SCDynamicStoreCopyConsoleUser; import sys; username = (SCDynamicStoreCopyConsoleUser(None, None, None) or [None])[0]; username = [username,""][username in [u"loginwindow", None, u""]]; sys.stdout.write(username + "\n");'`
DMG_PATH="/tmp/Google Chrome.dmg"
DMG_VOLUME_PATH="/Volumes/Google Chrome/"
APP_NAME="Google Chrome.app"
APP_PATH="/Applications/$APP_NAME"
APP_PROCESS_NAME="Google Chrome"
APP_INFO_PLIST="Contents/Info.plist"

## Use this snippet if you want the script to fail if the program is running
# if pgrep "$APP_PROCESS_NAME" &>/dev/null; then
#  echo 'Error - Chrome is currently running!'
# else
  curl --retry 3 -L "$DOWNLOAD_URL" -o "$DMG_PATH"
  hdiutil attach -nobrowse -quiet "$DMG_PATH"
  version=$(defaults read "$DMG_VOLUME_PATH/$APP_NAME/$APP_INFO_PLIST" CFBundleShortVersionString)
  printf "Installing $APP_PROCESS_NAME version %s" "$version"
  rm -rf /Applications/Google\ Chrome.app
  ditto -rsrc "$DMG_VOLUME_PATH/$APP_NAME" "$APP_PATH"
  hdiutil detach -quiet "$DMG_VOLUME_PATH"
  rm "$DMG_PATH"
# Uncomment row 31 if you are using the quit if running process
#fi